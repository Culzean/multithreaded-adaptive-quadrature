/* stack.h
 *
 * University of Edinburgh
 * 
 * Stack containing a single int.
 * This is a little bit on the over kill side, but I wish to use the is_empty function in regard to the number of workers.
 * A template solution is of course over engeineering. While simply reusing the double[2] stack with casts caused unusual segmentation fault errors.
 */

typedef struct stack_node_int_tag int_stack_node;
typedef struct int_stack_tag int_stack;

struct stack_node_int_tag {
  int     data;
  int_stack_node  *next;
};

struct int_stack_tag {
  int_stack_node     *top;
};


int_stack *new_int_stack();
void free_int_stack(int_stack *);

void push_int(int, int_stack *);
int pop_int (int_stack *);

int is_int_empty (int_stack *);
