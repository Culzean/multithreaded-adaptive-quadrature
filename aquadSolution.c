#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>
#include "stack.h"
#include "int_stack.h"

#define MAX_TASK 100
#define EPSILON 1e-3
#define F(arg)  cosh(arg)*cosh(arg)*cosh(arg)*cosh(arg)
#define A 0.0
#define B 5.0

#define NO_MORE_TASKS 1227777
#define MORE_TASKS 9
#define TERMINATION -1
#define SLEEPTIME 1

/*
Here we have a bag of tasks implementation, though this is specifically implemented to solve an adaptive quadrature problem, the pattern implemented here could readily be adapted to other problems. We shall take a look at the different components used in the pattern and the design choices followed in this implementation.

The farmer strategy is constructed around a series of while loops, these loops terminate once the bag is empty and there are no idle workers. We start the loop and ensure that the first task is ready in the bag, the idle workers and there correspending tags are made avaliable. There is a main loop to ensure the bag keeps running until the bag is empty. Once inside this loop the farmer should either be allocating tasks or waiting for a reply from the workers. And so we see there are checks if there are any outstanding tasks and idle workers, else we check if there are is an active worker with a task to submit.
 This is based on blocking sends and recieves. This is a simple and effective implementation for this pattern. The blcoking nature ensures we do not need to worry about syncronization between the farmer and workers, as we would have to if we used MPI_iSend or MPI_iRecv. If we used these we would not have to wait for the buffer to be released, this might allow the farmer or worker to proceed with some extra work before accessing the buffer. But this is a minor gain for a great deal of extra complexity. We might also consider using MPI_Sendrecv and complete the task distrbution and collection by the farmer or worker. It is not clear from the documentation if this is a blocking call or not. This offers a compact and readable way to complete the call, however if we are not carefully we might end up with a sequantially implementation. In this case we would send one task and wait for it to be returned on the MPI_Sendrecv. This would result in ønly one worker every being busy at once. A lot of work to complete a sequential implementation!
 The current implementation is clear, and even though the use if nested while loops is used, this is not too difficult to understand nor prone to error. We can also see from the load balance that the load is shared very evenly. If we run this multiple times with 8 workers we find that the difference between min and max number of tasks is average of 4.

This is simple problem but implemented in a multi-threaded environment we see there is a lot of data being passed around that must be kept track of. I have passed all data as MPI_DOUBLE because this gives the best accuracy for the problem we are solving. There are two lines of communication, the farmer sending out tasks and the workers sending in the results. There is a final communication where the tag system is used to instruct the workers to terminate, for simplicity this uses a dummy task to complete the call as the previous messages have been sent. In the task we send 5 double and receive 4 doubles for the answer, you can see below for notes on the indexing.


In the worker we use separate MPI_recv and MPI_send calls as a posed to MPI_Isend or MPI_Irecv. The design choice here is particularly to use blocking send and receives. We could use a non-blocking variant but would have to construct some work for the worker while they wait for a task, i.e. an empty while loop.

The worker is given the task of checking if the termination condition has been met. We could make this check on the farmer side but this would require some computation that we would rather plac eon the worker. The design choice here is have as much of the computation work for the problem inside of the worker. The termination state must then be communicated back to the farmer. This is achieved by returning a termination value. This works for this case but does impose some limitations for this bag of task implementation. We could as an alternative make use of termination tag to return and this would be more versatile.
 
 RUNNING
 
Please note this implemenation uses a int_stack.h and int_stack.c files to allow easy tracking of the curently idle workers. This will need to compiled with the other files in order to run the demo.
 
 /usr/lib64/openmpi/bin/mpicc -o aquadSolution aquadSolution.c stack.h stack.c int_stack.c int_stack.h -lm
 
 Othewise this should run as normal.


*/

int *tasks_per_process;

double farmer(int);

void worker(int);

double quad (double, double, double, double, double);


int main(int argc, char **argv ) {
  int i, myid, numprocs;
  double area, a, b;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if(numprocs < 2) {
    fprintf(stderr, "ERROR: Must have at least 2 processes to run\n");
    MPI_Finalize();
    exit(1);
  }

  if (myid == 0) { // Farmer
    // init counters
    tasks_per_process = (int *) malloc(sizeof(int)*(numprocs));
      for (i=0; i<numprocs; i++) {
      tasks_per_process[i]=0;
    }
  }

  //printf("Ready to start farming...\n");
  if (myid == 0) { // Farmer
    area = farmer(numprocs);
  } else { //Workers
    worker(myid);
  }

//make sure all threads are correctly terminated before displaying the result
usleep(100);

  if(myid == 0) {
    fprintf(stdout, "Area=%lf\n", area);
    fprintf(stdout, "\nTasks Per Process\n");
    for (i=0; i<numprocs; i++) {
      fprintf(stdout, "%d\t", i);
    }
    fprintf(stdout, "\n");
    for (i=0; i<numprocs; i++) {
      fprintf(stdout, "%d\t", tasks_per_process[i]);
    }
    fprintf(stdout, "\n");
    free(tasks_per_process);
  }
  MPI_Finalize();
  return 0;
}

double farmer(int numprocs) {
  // You must complete this function
	printf("Farmer start\n");
	int_stack *idle_stack = new_int_stack();
	stack *work_stack = new_stack();
	double task[5], result[4];
	double points[2];
	double answer = 0;
	double* temp;
	int tag, who;
	MPI_Status status;
	int idle_count = numprocs-1;
	int i = 0;

	//build first task for integral problem
	points[0] = A;	//left point
	points[1] = B;	//right point

    push(points, work_stack);
    
	//prepare workers
	for (i=1; i<(numprocs); i++) {
		push_int( i, idle_stack );
	}

//keep checking bag while we still have tasks
while (!is_empty(work_stack)) {
	//while there are idle workers and we still have tasks in the bag, then keep assigning work
	while( !is_int_empty(idle_stack) && !is_empty(work_stack) ) {
		temp = pop(work_stack);
		//who should we send the task to?
		who = pop_int(idle_stack);
        
		task[0] = temp[0];//left point
		task[1] = temp[1];//right point
		task[2] = F(temp[0]);//right y value
		task[3] = F(temp[1]);
		task[4] = ((F(temp[0])+F(temp[1])) * (temp[1]-temp[0])/2);
		
		MPI_Send( &task, 5, MPI_DOUBLE, who, MORE_TASKS, MPI_COMM_WORLD );
		idle_count--;
	}

	//other wise we should wait and recieve completed tasks
	while( idle_count < (numprocs-1) ) {

		//using blocking recv to ensure that we get a result before we attempt to do anything else
		MPI_Recv(&result, 4, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG,	
				MPI_COMM_WORLD, &status);

		who = status.MPI_SOURCE;
		tag = status.MPI_TAG;
		//The termination condition is communicated as a -1 value returned on all important values
		if ( result[1] == TERMINATION && result[2] == TERMINATION && result[3] == TERMINATION ) {
		//sum result
		answer += result[0];
		}
		else {
		  //prepare more tasks
		  points[0] = result[1];  points[1] = result[2];
		  push(points, work_stack);
		  points[0] = result[2]; points[1] = result[3];
		  push(points, work_stack);
		}

		//make sure worker is recorded as idle
		push_int( who, idle_stack );
		idle_count++;
		tasks_per_process[who]++;
	}

}


   printf("Farmer exits while loop and will collect final results \n");

//finally we must tell each worker that the task is completed, the message is send as a tag
   for (i=1; i<(numprocs); i++) {
     
     MPI_Send(&task, 5, MPI_DOUBLE, i, NO_MORE_TASKS, 
	                                    MPI_COMM_WORLD);
   }
   
	free_stack(work_stack);
	free_int_stack(idle_stack);

  return answer;
}

void worker(int mypid) {
  // You must complete this function
  printf("Worker start %d \n", mypid);
  int tasksdone = 0;
  double workdone  = 0;
  //collect as a result, the area, left right and mid points
  double result[4];
  //need 5 doubles for the task, left right, fleft, fright, lrarea
  double task[5];
  int tag = MORE_TASKS;
  MPI_Status status;
  
  //keep looping until the farmer instructs us that all tasks are completed
  while (tag != NO_MORE_TASKS) {
  
  //in this simulated multithreaded envirnoment we must wait for a short time
    	usleep(SLEEPTIME);
      	MPI_Recv(&task, 5, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD,
                                                        &status);
      	tag = status.MPI_TAG;
      	tasksdone++;
    	
      	workdone = quad(task[0], task[1], task[2], task[3], task[4]);
      
	//compare new area calculation to previous one. I had a bug in the termination check, I had attempted to compare this to result[0] but this assumes the thread is assinged the tasks in order and has the previous area stored locally! not true.
        if(fabs(workdone - task[4]) <= EPSILON) {
          //we have completed our approximation
          result[0] = workdone;
          result[1] = -1.0; result[2] = -1.0; result[3] = -1.0;
        } else {
          //store interim result and provide results for next task
          //the indexing for result is a little tricky
          //0 is answer, 1,2,3 are the points for the next two task - left, mid and right points.
          result[0] = workdone;
          result[1] = task[0]; result[3] = task[1];
          result[2] = task[0] + ((task[1] - task[0]) / 2); 
        }
        
      	MPI_Send(&result, 4, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
        
        //printf("Worker returns result %d %f %f %f %f \n", mypid, result[0],result[1],result[2],result[3]);

      //printf("Worker completed task and recived new task %d \n", mypid);
  }
  printf("Worker %d solved %d tasks totalling %d units of work \n", mypid, tasksdone, workdone);
}

//helper function to find solution for a given task.
double quad(double left, double right, double fleft, double fright, double lrarea) {
  double mid, fmid, larea, rarea;
  
  mid = (left + right) / 2;
  fmid = F(mid);
  larea = (fleft + fmid) * (mid - left) / 2;
  rarea = (fmid + fright) * (right - mid) / 2;
  return (larea + rarea);
}
