// Simple type for int_stack of doubles

#include <stdio.h>
#include <stdlib.h>
#include "int_stack.h"

// creating a new int_stack
int_stack * new_int_stack()
{
  int_stack *n;

  n = (int_stack *) malloc (sizeof(int_stack));

  n->top = NULL;
  
  return n;
}

// cleaning up after use
void free_int_stack(int_stack *s)
{
  free(s);
}

// Push data to int_stack s, data has to be an array of 2 doubles
void push_int (int data, int_stack *s)
{
  int_stack_node *n;
  n = (int_stack_node *) malloc (sizeof(int_stack_node));
  n->data  = data;

  if (s->top == NULL) {
    n->next = NULL;
    s->top  = n;
  } else {
    n->next = s->top;
    s->top = n;
  }
}

// Pop data from int_stack s
int pop_int (int_stack * s)
{
  int_stack_node * n;
  int data;
  
  if (s == NULL || s->top == NULL) {
    return -1;
  }
  n = s->top;
  s->top = s->top->next;
  data = n->data;
  free (n);

  return data;
}

// Check for an empty int_stack
int is_int_empty (int_stack * s) {
  return (s == NULL || s->top == NULL);
}
